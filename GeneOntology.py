#!/usr/bin/python3
# -*- coding: utf-8 -*-

#############
#  IMPORTS  #
#############

import TPGraphlib as gr # Graph library from part 1 of the project
import math
from pprint import pprint
import time
import argparse, sys
from functools import reduce
import numpy as np
import json
import copy
import os

#############
# FUNCTIONS #
#############

def load_OBO(filename):
    """
    Parse the OBO file and returns the graph.
    Obsolete terms are discarded and only is_a and part_of relationships are loaded.

    Extract of a file to be parsed:
    [Term]
    id: GO:0000028
    name: ribosomal small subunit assembly
    namespace: biological_process
    def: "The aggregation, arrangement and bonding together of constituent RNAs and proteins to form the small ribosomal subunit." [GOC:jl]
    subset: gosubset_prok
    synonym: "30S ribosomal subunit assembly" NARROW [GOC:mah]
    synonym: "40S ribosomal subunit assembly" NARROW [GOC:mah]
    is_a: GO:0022618 ! ribonucleoprotein complex assembly
    relationship: part_of GO:0042255 ! ribosome assembly
    relationship: part_of GO:0042274 ! ribosomal small subunit biogenesis
    """
    def parseTerm(lines):
        # Search for obsolete :
        for l in lines:
            if l.startswith('is_obsolete: true'): return
        # Otherwise create node :
        id = lines.pop(0)[4:].rstrip()
        term = gr.add_node(g,id)
        term['id'] = id
        term['type'] = 'GOTerm'
        for line in lines:
            # Attributes (name, namespace, def) :
            if line.startswith('name: '): term['name'] = line[6:]
            elif line.startswith('namespace: '): term['namespace'] = line[11:]
            elif line.startswith('def: '): term['def'] = line[5:]
            elif line.startswith('alt_id: '): g['alt_id'][ line[8:] ] = id # alternate ids
            # Relationships :
            elif line.startswith('is_a:'): # is_a
                parent = line[6:line.index('!')].rstrip()
                e = gr.add_edge(g,id, parent)
                e['type'] = 'is_a'
            elif line.startswith('relationship: part_of '): # part_of
                line = line[line.index('GO:'):]
                dest = line[:line.index(' ')]
                e = gr.add_edge(g,id, dest)
                e['type'] = 'part_of'
    
    g = gr.create_graph(directed=True, weighted=False)
    g['alt_id'] = {} # alternate GO ids
    with open(filename) as f: 
        line = f.readline().rstrip()
        # Skip header to reach 1st Term :
        while not line.startswith('[Term]'): 
            line = f.readline().rstrip()
        buff = []
        line = f.readline()
        stop = False
        while line and not stop:
            # Buffer lines until the next Term is found :
            line = line.rstrip()
            # New Term
            if line.startswith('[Term]'):
                # Next Term found: create corresponding node and edges in parseTerm and empty buffer :
                parseTerm(buff)
                buff=[]
            # Last Term :
            elif line.startswith('[Typedef]'):
                parseTerm(buff)
                stop=True
            # Or append to buffer :
            else:
                buff.append(line)
            line = f.readline()
    return g

def load_GOA(go, filename):
    """
    Parse a GOA file and add annotated gene products to previsouly loaded graph go.

    Extract of a file to be parsed:
    !gaf-version: 2.1
    !GO-version: http://purl.obolibrary.org/obo/go/releases/2016-10-29/go.owl
    UniProtKB  A5A605  ykfM      GO:0006974  PMID:20128927   IMP              P  Uncharacterized protein YkfM    YKFM_ECOLI|ykfM|b4586         protein taxon:83333  20100901  EcoCyc
    UniProtKB  A5A605  ykfM      GO:0016020  GO_REF:0000037  IEA              C  Uncharacterized protein YkfM    YKFM_ECOLI|ykfM|b4586         protein taxon:83333  20161029  UniProt
    UniProtKB  P00448  sodA      GO:0004784  GO_REF:0000003  IEA  EC:1.15.1.1 F  Superoxide dismutase [Mn]       SODM_ECOLI|sodA|JW3879|b3908  protein taxon:83333  20161029  UniProt
    UniProtKB  P00393  ndh  NOT  GO:0005737  PMID:6784762    IDA              C  NADH dehydrogenase              DHNA_ECOLI|ndh|JW1095|b1109   protein taxon:83333  20100621  EcoliWiki
        0        1       2   3       4             5          6        7      8             9                              10
                 id    name        go_id               evidence-codes                     desc                           aliases
    """
    names = {}
    go['names'] = names # gene names or gene product names (column 3)
    with open(filename) as f: 
        line = f.readline()
        while line:
            if not line.startswith('!'):
                cols = line.rstrip().split('\t')
                id = cols[1]
                go_id = cols[4]
                if go_id not in go['nodes']: # GOTerm not found search alternate ids
                    if go_id in go['alt_id']: # success
                        go_id = go['alt_id'][go_id] # replace term
                    else: # warn user
                        print('Warning: could not attach a gene product (%s) to a non existing GO Term (%s)' % (id, go_id))
                if go_id in go['nodes']:
                    # create node for gene product if not already present
                    if id not in go['nodes']:
                        g = gr.add_node(go,id)
                        g['id'] = id
                        g['type'] = 'GeneProduct'
                        names[cols[2]] = id
                    # create or update gene product attributes
                    gp = go['nodes'][id]            
                    gp['name'] = cols[2]
                    gp['desc'] = cols[9]
                    gp['aliases'] = cols[10]
                    # attach gene product to GOTerm
                    go_term = go['nodes'][go_id]
                    e = gr.add_edge(go, id, go_id)
                    e['type'] = 'annotation'
                    if 'evidence-codes' not in e: e['evidence-codes'] = []
                    e['evidence-codes'].append( cols[6] )
                else: # go_id or alt_id not found in GOTerms
                    print('Error: could not attach a gene product (%s) to non existing GO Term (%s)' % (id, go_id))
            line = f.readline()


def get_root(go):
    """
        Identifier les noeuds sans arc entrant -> pour intialiser le parcours du graphe par la suite
        res: ["GO:0008150", "GO:0003674", "GO:0005575"]
    """

    wlist = []

    for nid in go['nodes']:

        if not go['edges'][nid]:
            wlist.append(nid)
    
    return wlist

def get_sink(go):
    """
        Identifier les noeuds sans arc sortant -> pour intialiser le parcours du graphe par la suite
        res: ["GO:0008150", "GO:0003674", "GO:0005575"]
    """

    wlist = []

    LParentsGO = [ list(dict.keys(h)) for h in go['edges'].values() ]   
   
    ParentsGO = [] 
    
    for l in LParentsGO:
        ParentsGO += l
    
    ParentsGO = set(np.unique(ParentsGO))

    nnodes = set([ nid for nid in go['nodes'] if go['nodes'][nid]['type'] != 'GeneProduct' ])

    return list(nnodes - ParentsGO)


# def parcours(go, edges, goid, lgp = [], recursive=False, maxdepth=-1, i=0): # lgp -> la liste des GeneProduct associées au terme GO "id"
    
#     # Enfant du noeud
#     children = sorted([ eid for eid in edges if goid in edges[eid].keys() ]) # on récupère la liste des enfants du noeud 'id' (l) 

#     print("LEVEL ("+str(i)+") CURRENT NODE : ",goid)
#     #print("  - CHILDREN : ", children)
#     print("  - CHILDREN : ", children)

#     if len(children) > 0:

#         for child in children:

#             if go['nodes'][child]['type'] == "GeneProduct":

#                 # if child in edges.keys():
#                 #     del edges[child]

#                 if child not in lgp:
#                     lgp.append(child)

#             else:
#                 # if child in edges.keys():
#                 #    del edges[child]
                
#                 if recursive == True :

#                     i+=1

#                     if maxdepth < 0 or i < maxdepth:

#                         parcours(go, edges, child, lgp, recursive=recursive, maxdepth=maxdepth, i=i)
  
#     return lgp  

def parcours(go, goid, Hannot, maxdepth=-1, i=0): # lgp -> la liste des GeneProduct associées au terme GO "id"
    
    # Enfant du noeud
    # children = sorted([ eid for eid in edges if goid in edges[eid].keys() ]) # on récupère la liste des enfants du noeud 'id' (l) 

    parents = [k for k in [h for h in go['edges'][goid]] if go['nodes'][k]['type'] != 'GeneProduct']

    print("LEVEL ("+str(i)+") CURRENT NODE : ",goid)
    #print("  - CHILDREN : ", children)
    print("  - PARENTS : ", parents)

    if len(parents) > 0:

        for p in parents:

            if goid in Hannot.keys():

                if p not in Hannot.keys():
                    Hannot[p] = copy.deepcopy(Hannot[goid])

                Hannot[p] += Hannot[goid]
                Hannot[p] = sorted(list(np.unique(Hannot[p])))

            i+=1

            if maxdepth < 0 or i < maxdepth:

                parcours(go, p, Hannot=Hannot, maxdepth=maxdepth, i=i)
  
    return Hannot  

    
def get_nodes(go):
    '''
    Génère la liste des noeuds triés en fonction du type de noeud (GOTerm ou GeneProduct)
    '''
    lnodes = {}
    lnodes['GO']=[]
    lnodes['GP']=[]

    for id in go['nodes']:
        if go['nodes'][id]['type'] == 'GeneProduct':
            lnodes['GP'].append(id)
        elif go['nodes'][id]['type'] == 'GOTerm':
            lnodes['GO'].append(id)

    return(lnodes)


def get_GOproducts(go, recursive=False, goids=[], output_tab="go_gp.sets", sep=',', maxdepth=-1):

    '''
    Retourne la liste des GeneProducts pour une annotation GO donnée (GOid)
    Les GeneProducts retournés correspondront donc soit :
     - aux produits directs (recursive=False)
     - à l'ensemble des produits de l'annotation et des annotations descendantes (recursive=True)
    '''

    os.system('if [ -e  '+output_tab+' ]; then rm '+output_tab+"; fi")

    GOfam = {}

    if len(goids) == 0:
        nodes = [ n for n in go['nodes'] if go['nodes'][n]['type'] == "GeneProduct"]
    else:
        nodes = goids
    
    with open(output_tab, 'w') as outfile:

        for n in nodes:

            for k in go['edges'][n].keys():
                
                if go['edges'][n][k]['type'] == 'annotation':

                    if k not in GOfam.keys():
                        GOfam[k] = [n]
                    else:
                        GOfam[k].append(n)

        if recursive == False:

            for k in GOfam.keys():

                outfile.write(k+"\t"+go['nodes'][k]['namespace']+": "+go['nodes'][k]['name']+"\t"+sep.join(GOfam[k])+"\n")
        
        else:

            sinks = sorted(get_sink(go))

            print(len(sinks))
            print(len([ n for n in go['nodes'] if go['nodes'][n]['type'] != "GeneProduct"]))

            for n in sinks:

                print("#############################\\nNODE "+n)

                parcours(go, n, Hannot=GOfam, maxdepth=maxdepth)

                print(n+"\t"+go['nodes'][n]['namespace']+": "+go['nodes'][n]['name']+"\n=======================================\n")
            
            for k in GOfam.keys():

                outfile.write(k+"\t"+go['nodes'][k]['namespace']+": "+go['nodes'][k]['name']+"\t"+sep.join(GOfam[k])+"\n")

#############
# LIB TESTS #
#############

parser = argparse.ArgumentParser(description="Gene Ontology parser")

parser.add_argument('-g','--gene_ontology', help = "Gene Ontology OBO file", required=True)

parser.add_argument('-i','--input_genes', help = "Input genes GOA file names", required=True)

parser.add_argument('-l', '--GOterms', help="List of GO terms for which gene associations are screened", default="", required=False)

args = parser.parse_args()

if __name__ == "__main__":

    print('Load Gene Ontology...')

    go = load_OBO(args.gene_ontology)

    print('Load organism genes on GO...')

    load_GOA(go, args.input_genes)

    # Direct
    get_GOproducts(go, recursive=False, output_tab="go_gp_direct.sets")

    # Implicite
    get_GOproducts(go, recursive=True, output_tab="go_gp_undirect.sets")