#!/usr/bin/env python
# Copyright 2018 BARRIOT Roland
# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import numpy as np
from os.path import isfile
import random as rd

# FUNCTIONS

# compute coverage

def coverage(q, t, c, g):

	if c == 0:
		return 1
	elif c == q and c == t:
		return 0
	else:
		return 1 - ((c/q) * (c/t))


# SCRIPT PARAMETERS
# e.g. ./blastset_gen.py --sets EcolA.biocyc.sets -
parser = argparse.ArgumentParser(description='Search enriched terms/categories in the provided (gene) set')
parser.add_argument('-t', '--sets', required=True, help='Target sets file (categories).')
parser.add_argument('-m', '--mutrate', required=False, type=float, default=0.2, help='Random positions in selected families')
parser.add_argument('-n', '--nbfamilies', required=False, type=int, default=100, help='Number of selected families in generated set')
param = parser.parse_args()

def mutate_sample(s, genelist):

    for i in rd.sample(range(len(s['elements'])), int(param.mutrate*len(s['elements']))):

        s['elements'][i] = rd.choice(genelist)

    return s
    

# FAMILIES

samples = []
allgenes = []

# LOAD SET FILE

with open(param.sets, 'r') as infile:

    for l in infile.readlines():

        id, nameall, l = l.replace('\n','').split("\t")
        
        s = {}
        s['id'] = id
        s['name'] = nameall
        s['elements'] = l.split(',')

        samples.append(s)

l=[s['elements'] for s in samples]

allgenes = list(np.unique([ v for sl in l for v in sl ]))

# SELECT 'n' FAMILIES

sel_sample = [ mutate_sample(rd.choice(samples), allgenes) for i in range(param.nbfamilies) ]

with open('sample_nb'+str(param.nbfamilies)+"_mut"+str(param.mutrate)+".sets", 'w') as outfile:

    for s in sel_sample:

        line=s['id']+"\t"+s['name']+"\t"+','.join(s['elements'])+"\n"

        outfile.write(line)