# The script generate datasets used to compare different kinds of measures

for ft in $(echo "direct undirect"); do

    echo "$ft .sets file ..."

    # Generate test datasets

    for mr in $(0.1 0.2 0.4 0.5 0.6 0.7 0.8 0.9); do

        python3 blastset_sample.py -t Resultats/P_falciparum/$ft/test_samples/go_gp.sets -m $mr -n 100

    done;

    # Analysis of test datasets

    echo $(ls Resultats/P_falciparum/$ft/test_samples/sample_*|sort|sed 's/\.sets//g'|sed 's/test.\+mut//g')|sed 's/ /\t/g' > Resultats/P_falciparum/$ft/results_count.txt

    for t in $(echo "coverage chi2 binomial hypergeometric"); do
        resline="$t"
        for f in $(ls Resultats/P_falciparum/$ft/test_samples/sample_*|sort); do
            ratio=`echo $f|sed 's/\.sets//g'|sed 's/test.\+mut//g'`
            res_file=results_$ratio.txt;
            touch $res_file;
            count=0;
            while read l; do
                goid=`echo $l|sed 's/\s\+.\+$//g'`;
                elements=`echo $l|sed 's/^.\+ //g'`;
                tmp_count=`python3 blastset.py -q $elements -t Resultats/P_falciparum/$ft/test_samples/go_gp.sets -c -m $t| grep -c $goid`
                count=$(echo "$count+$tmp_count"|bc)
                echo $count
            done < $f;
            perc=$(echo "$count/100"|bc -l)
            resline="$resline\t$perc"
            echo "$resline"
        done;
        echo "$resline"
        echo "$resline" >> results_count.txt
    done;

done;